# What's the Weather Like

This project contains two notebooks.
The first notebook, WeatherPy, is intended to visualize the weather of 500+ cities across the world of varying distance from the equator. 
The second notebook, VacationPy, maps the humidity of the cities in the first notebook, and adds pins to ideal locations

## Getting Started

From the project root run the following command line statement: 'jupyter-notebook.exe'.
Once Jupyter opens, you can navigate to the WeatherPy or VacationPy folders and open the notebooks in the respective directories. 
Please note that the WeatherPy notebook will need to be run BEFORE the VactionsPy notebook can run. 
In order to run either notebook, you will need to create a file in both the WeatherPy and VacationPy folder named api_kyes.py. This file needs to store two variables, 'weather_api_key' (which will store your OpenWeatherMap API key) and 'gmap' which will store you google maps API key. You will need to have both Google Maps and Google Places enabled as well. 
Once a notebook is open in Jupyter, select the first cell and press SHIFT + ENTER to run it.
This will also move the cell selection to the next cell down. The remainder of the notebook results
can be viewed by continuing to press SHIFT + ENTER in each cell.

### Prerequisites

This project requires Jupyter Notebook to be installed. Installation instructions can be found [here](https://jupyter.org/install).
You will also need the gmaps extension. Installation instructions can be found here: https://jupyter-gmaps.readthedocs.io/en/latest/install.html

### Installing

This project relies on the following dependencies:
- scipy==1.1.0
- pandas==0.23.4
- matplotlib==3.0.2
- numpy==1.15.4
- requests==2.21.0
- gmaps==0.9.0

These packages can be installed via pip or Anaconda

## Running the tests

Automated tests are included in the Jupyter Notebook file in the form of assert statements. None of the 
assert statements should fail as you execute the notebook.

### Break down into end to end tests

The scripts included in this project are self-contained. The assert statements act as end-to-end tests.

## Deployment

The scripts included in this project are self-contained and intended to be used for one-off runs. Assuming you feel these scripts should be deployed to production, no additional steps are needed beyond those stated in 'Getting Started' above.

## Built With

* [Python](http://www.python.org) - Language used
* [Jupyter](https://jupyter.org) - IDE used
* [Pandas](https://pandas.pydata.org) - Data Manipulation Library
* [Matplotlib](https://matplotlib.org) - Data Visualization Library
* [Numpy](https://numpy.org) - Data Manipulation Library
* [SciPy](https://www.scipy.org) - Statistics Library
* [Gmap](https://github.com/pbugnion/gmaps) - Map Visualization Library

## Versioning

We use [SemVer](http://semver.org/) for versioning. The version is currently 1.0.0.

## Authors

* **Michael Whitmore** - *All work* - [mwhitmorelaw](https://gitlab.com/mwhitmorelaw)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Guido van Rossum
